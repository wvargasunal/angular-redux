import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { DestinoViaje } from '../../model/destino-viaje';
import { DestinosApiClient } from '../../model/destinos-api-client';
import { Store } from '@ngrx/store';
import { AppState } from '../../app.module';

@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.css'],
  providers: [ DestinosApiClient ]
})
export class ListaDestinosComponent implements OnInit {
@Output() onItemAdded: EventEmitter<DestinoViaje>;
updates: string[];
all;
  constructor(public destinosApiClient: DestinosApiClient, public store: Store<AppState>) {
   this.onItemAdded = new EventEmitter();
   this.updates = [];
   this.store.select(state => state.destinos.favorito)
   .subscribe(d => {
     if ( d != null) {
      this.updates.push('Se ha elegido a ' + d.nombre);
    }
    });
   store.select(state => state.destinos.items).subscribe(items => this.all = items);
  }

  ngOnInit() {
  }
 agregado(d: DestinoViaje) {
  this.destinosApiClient.add(d);
  this.onItemAdded.emit(d);
 }
 elegido(e: DestinoViaje) {
  this.destinosApiClient.elegir(e);
 }

 getAll() {

 }
}
