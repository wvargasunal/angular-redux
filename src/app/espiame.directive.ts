import { Directive, OnInit, OnDestroy } from '@angular/core';

@Directive({
  selector: '[appEspiame]'
})
export class EspiameDirective implements OnInit, OnDestroy {
static maxtId = 0;
log = (msg: string) => console.log('evanto #${EspiameDirective.nextId++} ${msg}');
ngOnInit() { this.log('########********* onInit'); }
ngOnDestroy() { this.log('##############*********** onDestroy'); }

}
