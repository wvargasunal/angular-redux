import { DestinoViaje } from './destino-viaje';
import { AppState, APP_CONFIG, AppConfig, db } from '../app.module';
import { HttpClientModule, HttpClient, HttpHeaders, HttpRequest, HttpResponse } from '@angular/common/http';
import { Store } from '@ngrx/store';
import { NuevoDestinoAction, ElegidoFavoritoAcction } from './destinos-viajes-state';
import { Injectable, forwardRef, Inject } from '@angular/core';
import { inject } from '@angular/core/testing';

@Injectable()
export class DestinosApiClient {
    destinos: DestinoViaje[] = [];  // nuevo
    constructor(
      private store: Store<AppState>,
      @Inject(forwardRef(() => APP_CONFIG)) private config: AppConfig,
      private http: HttpClient
      )
      { 
      this.store
      .select(state => state.destinos)
      .subscribe((data) => {
        console.log('destinos sub store');
        console.log(data);
        this.destinos = data.items;
      });
      this.store
      .subscribe((data) => {
        console.log('all store');
        console.log(data);
      });
    }
    
 add(d: DestinoViaje){
   const header: HttpHeaders = new HttpHeaders({'X-API-TOKEN': 'token-seguridad'});
   const req = new HttpRequest('POST', this.config.apiEndpoint + '/my', { nuevo: d.nombre }, { headers: header});
   this.http.request(req).subscribe((data: HttpResponse<{}>) => {
     if (data.status === 200) {
       this.store.dispatch(new NuevoDestinoAction(d));
       const myDb = db;
       myDb.destinos.add(d);
       console.log('todos los destinos de la deb');
       myDb.destinos.toArray().then(destinos => console.log(destinos))
     }
   });
 }
 elegir(d: DestinoViaje) {
 this.store.dispatch(new ElegidoFavoritoAcction(d));
 }
// nuevo
 getById(id: string): DestinoViaje {
    return this.destinos.filter(function(d) { return d.id.toString() === id; })[0];
  }
  // nuevo
  getAll(): DestinoViaje[] {
    return this.destinos;
  }
}
